﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {

    public Piece piece;

	// Use this for initialization
	void Start () {
        if (GetComponentInChildren<Piece>() != null)
        {
            piece = GetComponentInChildren<Piece>();
            piece.prevValidCell = this;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (GetComponentInChildren<Piece>() != null)
        {
            piece = GetComponentInChildren<Piece>();
        }
        else
        {
            piece = null;
        }
    }
}
