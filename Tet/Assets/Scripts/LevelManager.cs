﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DIR
{
    NULL,
    UP,
    LEFT,
    RIGHT,
}

[System.Serializable]
public struct Shape
{
    public string shapeName;
    public int horizontalReq;
    public int verticalReq;
    public int forDiagReq;
    public int backDiagReq;
}

public struct PieceCounter
{
    public int horizontal;
    public int vertical;
    public int forDiag;
    public int backDiag;
}

public class LevelManager : MonoBehaviour {

    public TextAsset upJSON;
    [HideInInspector]
    public Shape upShape;

    public TextAsset leftJSON;
    [HideInInspector]
    public Shape leftShape;

    public TextAsset rightJSON;
    [HideInInspector]
    public Shape rightShape;

    public PieceCounter upPieces;
    public bool upComplete = false;

    public PieceCounter leftPieces;
    public bool leftComplete = false;

    public PieceCounter rightPieces;
    public bool rightComplete = false;


	// Use this for initialization
	void Start () {
        LoadShapes();
	}
	
	// Update is called once per frame
	void Update () {
        CheckIfSidesComplete();
	}

    void LoadShapes()
    {
        upShape = JsonUtility.FromJson<Shape>(upJSON.ToString());
        print(upShape.shapeName);
        print(upShape.horizontalReq);
    }

    public void SendPiece(Piece piece, DIR d)
    {
        switch (piece.id)
        {
            case ID.HORIZONTAL:
                if (d == DIR.UP) upPieces.horizontal++;
                else if (d == DIR.LEFT) leftPieces.horizontal++;
                else if (d == DIR.RIGHT) rightPieces.horizontal++;
                break;
            case ID.VERTICAL:
                if (d == DIR.UP) upPieces.vertical++;
                else if (d == DIR.LEFT) leftPieces.vertical++;
                else if (d == DIR.RIGHT) rightPieces.vertical++;
                break;
            case ID.FORWARDDIAG:
                if (d == DIR.UP) upPieces.forDiag++;
                else if (d == DIR.LEFT) leftPieces.forDiag++;
                else if (d == DIR.RIGHT) rightPieces.forDiag++;
                break;
            case ID.BACKWARDDIAG:
                if (d == DIR.UP) upPieces.backDiag++;
                else if (d == DIR.LEFT) leftPieces.backDiag++;
                else if (d == DIR.RIGHT) rightPieces.backDiag++;
                break;
            default:
                break;
        }
    }

    void CheckIfSidesComplete()
    {
        if (!upComplete)
        {
            if (upPieces.horizontal >= upShape.horizontalReq && 
                upPieces.vertical >= upShape.verticalReq &&
                upPieces.forDiag >= upShape.forDiagReq &&
                upPieces.backDiag >= upShape.backDiagReq)
            {
                upComplete = true;
            }
        }

        if (!leftComplete)
        {
            if (leftPieces.horizontal >= leftShape.horizontalReq &&
                leftPieces.vertical >= leftShape.verticalReq &&
                leftPieces.forDiag >= leftShape.forDiagReq &&
                leftPieces.backDiag >= leftShape.backDiagReq)
            {
                leftComplete = true;
            }
        }

        if (!rightComplete)
        {
            if (rightPieces.horizontal >= rightShape.horizontalReq &&
                rightPieces.vertical >= rightShape.verticalReq &&
                rightPieces.forDiag >= rightShape.forDiagReq &&
                rightPieces.backDiag >= rightShape.backDiagReq)
            {
                rightComplete = true;
            }
        }
    }
}
