﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public GameObject selectedPiece;

    private Vector3 prevMousePos;
    private Vector3 mouseDelta;

    private Vector3 selectedPieceOffset;

    private List<Piece> attachedPieces;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //if (attachedPieces != null) print(attachedPieces.Count);

        if (Input.GetMouseButtonUp(0))
        {
            GameManager.manager.UpdateAllPieces();

            if (selectedPiece != null)
            {
                selectedPiece = null;
                selectedPieceOffset = Vector3.zero;

                attachedPieces = null;

            }

        }

        if (selectedPiece != null)
        {
            UpdateSelectedPiecePos();

            if (attachedPieces != null)
            {
                UpdateAttachedPieces();
            }
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 pos = Input.mousePosition;
            pos = Camera.main.ScreenToWorldPoint(pos);
            pos.z = -float.MaxValue;
            RaycastHit2D[] hits = Physics2D.RaycastAll(pos, Vector3.zero);

            for (int i = 0; i < hits.Length; i++)
            {
                if (selectedPiece == null && hits[i].collider.gameObject.GetComponent<Cell>() != null
                    && hits[i].collider.gameObject.GetComponent<Cell>().piece != null)
                {
                    selectedPiece = hits[i].collider.gameObject.GetComponent<Cell>().piece.gameObject;
                    selectedPieceOffset = selectedPiece.transform.position;

                    Cell c = GameManager.manager.GetCellAt(selectedPiece.transform.position);
                    attachedPieces = GameManager.manager.AttachPieces(GameManager.manager.GetCellsAlongAxis(c));
                    SortAttachedPieces();
                    break;
                }
            }
        }

        mouseDelta = Input.mousePosition - prevMousePos;
        prevMousePos = Input.mousePosition;

	}

    void UpdateSelectedPiecePos()
    {
        Piece p = selectedPiece.GetComponent<Piece>();

        Vector3 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);

        Vector3 newPos = new Vector3(mousePos.x * p.axis.x, mousePos.y * p.axis.y, 0);

        Vector3 effectiveOffset = new Vector3(selectedPieceOffset.x * p.axis.y, selectedPieceOffset.y * p.axis.x, 0);
        newPos += effectiveOffset;

        if (p.axis.x == 1 && p.axis.y == 1)
        {
            float intercept = p.transform.position.y - p.transform.position.x;

            Vector3 pointOne = new Vector3(mousePos.x, mousePos.x + intercept, newPos.z);
            Vector3 pointTwo = new Vector3(mousePos.y - intercept, mousePos.y, newPos.z);

            //print(intercept);

            if (true)
            {
                newPos = new Vector3((pointOne.x - pointTwo.x) / 2f + pointTwo.x, (pointOne.y - pointTwo.y) / 2f + pointTwo.y, newPos.z);
            }
        }

        if (p.axis.x == -1 && p.axis.y == 1)
        {
            Vector3 pointOne = new Vector3(-mousePos.y, mousePos.y, newPos.z);
            Vector3 pointTwo = new Vector3(mousePos.x, -mousePos.x, newPos.z);

            if (pointOne.x > pointTwo.x)
            {
                newPos = new Vector3((pointOne.x - pointTwo.x) / 2f, (pointOne.y - pointTwo.y) / 2f, newPos.z);
            }
            else
            {
                newPos = new Vector3((pointTwo.x - pointOne.x) / 2f, (pointTwo.y - pointOne.y) / 2f, newPos.z);
            }
        }

        selectedPiece.transform.position = newPos;
    }

    void UpdateAttachedPieces()
    {
        int selectedIndex = -1;

        //loop through pieces on the right side of selected piece, compare distances to prev piece and push it along the selected piece axis
        //use vector dot to push the piece in the right direction from the axis

        for (int i = 0; i < attachedPieces.Count; i++)
        {
            if (attachedPieces[i].gameObject == selectedPiece)
            {
                selectedIndex = i;
            }
        }

        if (selectedIndex == -1) return;

        for (int i = selectedIndex; i < attachedPieces.Count; i++)
        {
            if (attachedPieces[i].gameObject != selectedPiece)
            {
                if (Vector3.Distance(attachedPieces[i].transform.position, attachedPieces[i - 1].transform.position) < GameManager.manager.pieceMaxDist)
                {
                    Vector3 desiredPosition = attachedPieces[i - 1].transform.position + selectedPiece.GetComponent<Piece>().axis.normalized * GameManager.manager.pieceMaxDist;

                    attachedPieces[i].transform.position = desiredPosition;
                }
            }
        }
        for (int i = selectedIndex; i >= 0; i--)
        {
            if (attachedPieces[i].gameObject != selectedPiece)
            {
                if (Vector3.Distance(attachedPieces[i].transform.position, attachedPieces[i + 1].transform.position) < GameManager.manager.pieceMaxDist)
                {
                    Vector3 desiredPosition = attachedPieces[i + 1].transform.position - selectedPiece.GetComponent<Piece>().axis.normalized * GameManager.manager.pieceMaxDist;

                    attachedPieces[i].transform.position = desiredPosition;
                }
            }          
        }
    }

    void SortAttachedPieces()
    {
        if (selectedPiece.GetComponent<Piece>().id == ID.VERTICAL)
        {
            attachedPieces.Sort((x,y) => x.transform.position.y.CompareTo(y.transform.position.y));
        }
        else
        {
            attachedPieces.Sort((x, y) => x.transform.position.x.CompareTo(y.transform.position.x));
        }
    }
}
