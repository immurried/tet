﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ID
{
    HORIZONTAL,
    VERTICAL,
    FORWARDDIAG,
    BACKWARDDIAG,
}

public class Piece : MonoBehaviour {

    [HideInInspector]
    public Vector3 axis;
    public ID id;
    public Cell prevValidCell;
    public Vector3 posDeltaSinceLastAction;

    public bool active = true;

    [HideInInspector]
    public string pieceID;

	// Use this for initialization
	void Start () {
        switch (id)
        {
            case ID.HORIZONTAL:
                axis = Vector3.right;
                break;
            case ID.VERTICAL:
                axis = Vector3.up;
                break;
            case ID.FORWARDDIAG:
                axis = new Vector3(1, 1, 0);
                break;
            case ID.BACKWARDDIAG:
                axis = new Vector3(-1, 1, 0);
                break;
            default:
                break;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Deactivate()
    {
        active = false;
    }
}
