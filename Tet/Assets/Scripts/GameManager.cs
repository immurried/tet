﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager manager;
    public LevelManager levelManager;
    public int gridLength;
    public float pieceMaxDist;

    public GameObject grid;
    public Cell[] cells;
    //[HideInInspector]
    public List<List<Cell>> cellMatrix;

    private List<Piece> pieces;
    private List<Piece> sentPieces;
    private List<Piece> attachedPieces;

	// Use this for initialization
	void Start () {
        manager = this;

        cells = new Cell[gridLength * gridLength];

        GetCellsFromGrid();

        GetPiecesFromGrid();

        cellMatrix = FormCellMatrixFromList();

        sentPieces = new List<Piece>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void GetCellsFromGrid()
    {
        cells = grid.GetComponentsInChildren<Cell>();
    }

    public Cell GetClosestCell(Vector3 point, float radius)
    {
        Cell c = null;

        float shortestDist = float.MaxValue;
        for (int i = 0; i < cells.Length; i++)
        {
            float dist = Vector3.Distance(point, cells[i].transform.position);

            if (dist < radius && dist < shortestDist)
            {
                shortestDist = dist;
                c = cells[i];
            }
        }

        return c;
    }

    public Cell GetCellAt(Vector2 pos)
    {
        Vector3 p = pos;

        p.z = -float.MaxValue;
        RaycastHit2D[] hits = Physics2D.RaycastAll(pos, Vector3.zero);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.gameObject.GetComponent<Cell>() != null)
            {
                return hits[i].collider.gameObject.GetComponent<Cell>();
            }
        }

        return null;
    }

    public void UpdateAllPieces()
    {
        for (int i = 0; i < pieces.Count; i++)
        {
            if (!pieces[i].active)
            {
                break;
            }

            Cell c = GetCellAt(pieces[i].transform.position);

            pieces[i].posDeltaSinceLastAction = pieces[i].transform.position - pieces[i].prevValidCell.transform.position;

            if (c != null)
            {
                pieces[i].transform.position = c.transform.position;
                pieces[i].transform.parent = c.transform;
                pieces[i].prevValidCell = c;
            }

            if (c == null)
            {
                pieces[i].transform.parent = null;

                DIR dirToSend = DIR.NULL;

                if (pieces[i].posDeltaSinceLastAction.y > 0) dirToSend = DIR.UP;
                if (pieces[i].posDeltaSinceLastAction.x > 0) dirToSend = DIR.RIGHT;
                if (pieces[i].posDeltaSinceLastAction.x < 0) dirToSend = DIR.LEFT;

                print(dirToSend);


                SendPiece(pieces[i], dirToSend);
            }
        }
    }

    public List<Cell> GetCellsAlongAxis(Cell origin)
    {
        if (origin.piece == null) return null;

        List<Cell> cellsOnAxis = new List<Cell>();
        Vector3 axis = origin.piece.axis;

        int cellX = -1;
        int cellY = -1;

        for (int i = 0; i < cellMatrix.Count; i++)
        {
            for (int j = 0; j < cellMatrix[i].Count; j++)
            {
                if (origin == cellMatrix[i][j])
                {
                    cellX = i;
                    cellY = j;
                    break;
                }
            }
            
        }

        if (axis.x == 1 && axis.y == 0)
        {
            for (int i = 0; i < gridLength; i++)
            {
                cellsOnAxis.Add(cellMatrix[i][cellY]);
            }
        }
        if (axis.x == 0 && axis.y == 1)
        {
            for (int i = 0; i < gridLength; i++)
            {
                cellsOnAxis.Add(cellMatrix[cellX][i]);
            }
        }
        if (axis.x == 1 && axis.y == 1)
        {
            while (cellX > 0 && cellY < gridLength)
            {
                cellX--;
                cellY++;
            }

            while (cellX < gridLength && cellY >= 0)
            {
                cellsOnAxis.Add(cellMatrix[cellX][cellY]);
                cellX++;
                cellY--;
            }
        }
        if (axis.x == 1 && axis.y == -1)
        {
            while (cellX > 0 && cellY > 0)
            {
                cellX--;
                cellY--;
            }

            while (cellX < gridLength && cellY < gridLength)
            {
                cellsOnAxis.Add(cellMatrix[cellX][cellY]);
                cellX++;
                cellY++;
            }
        }

        return cellsOnAxis;
    }

    private void GetPiecesFromGrid()
    {
        pieces = new List<Piece>();

        Piece[] p = grid.GetComponentsInChildren<Piece>();

        for (int i = 0; i < p.Length; i++)
        {
            pieces.Add(p[i]);
        }
    }

    private List<List<Cell>> FormCellMatrixFromList()
    {
        List<List<Cell>> matrix = new List<List<Cell>>();

        for (int i = 0; i < gridLength; i++)
        {
            matrix.Add(new List<Cell>());
        }

        for (int i = 0; i < cells.Length; i++)
        {
            matrix[i % 3].Add(cells[i]);
        }
        

        return matrix;
    }

    public List<Piece> AttachPieces(List<Cell> cells)
    {
        List<Piece> pieces = new List<Piece>();

        for (int i = 0; i < cells.Count; i++)
        {
            if (cells[i].piece != null)
            {
                pieces.Add(cells[i].piece);
            }
        }

        return pieces;
    }

    void SendPiece(Piece piece, DIR d)
    {
        levelManager.SendPiece(piece, d);

        sentPieces.Add(piece);
    }

    void RemoveAllDeactivatedPieces()
    {
        for (int i = 0; i < sentPieces.Count; i++)
        {
            if (sentPieces[i].active)
            {
                pieces.Remove(sentPieces[i]);
                sentPieces[i].Deactivate();
            }

        }
    }
}
